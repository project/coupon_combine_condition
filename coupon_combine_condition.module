<?php

/**
 * Implements hook_inline_conditions_info
 */
function coupon_combine_condition_inline_conditions_info() {

  // Condition used for Order Discounts
  $conditions['coupon_combine_condition_order_no_other_offer'] = array(
    'label' => t('Coupon combine condition'),
    'entity type' => 'commerce_line_item',
    'callbacks' => array(
      'configure' => 'coupon_combine_condition_no_other_offer_configure',
      'build' => 'coupon_combine_condition_no_other_offer_build',
    ),
  );

  // Condition used for Product Discounts
  $conditions['coupon_combine_condition_line_item_no_other_offer'] = array(
    'label' => t('Coupon combine condition'),
    'entity type' => 'commerce_order',
    'callbacks' => array(
      'configure' => 'coupon_combine_condition_no_other_offer_configure',
      'build' => 'coupon_combine_condition_no_other_offer_build',
    ),
  );

  return $conditions;
}

/**
 * Implements hook_form_alter
 */
function coupon_combine_condition_form_alter(&$form, &$form_state, $form_id) {
  if(true || $form_id == 'commerce_discount_edit_product_discount_form') {
    // Add our custom CSS because for some reason using #attached on our custom field doens't work.
    $css_path = '/' . drupal_get_path('module', 'coupon_combine_condition') . '/coupon_combine_condition.css';
    $form['#attached']['css'][] = array(
      'data' => $css_path,
      'type' => 'file',
      'every_page' => false,
      'group' => CSS_SYSTEM,
      'preprocess' => false,
    );
  }
}

/**
 * Rules callback when evaluating our condition for a commerce_discount.
 *
 * @param object EntityDrupalWrapper $wrapper
 * @param array $values
 * @return bool
 */
function coupon_combine_condition_no_other_offer_build(EntityDrupalWrapper $wrapper, $values) {
  // Default to true because the real validation happens in hook_commerce_coupon_redeem_error_alter.
  // Note that hook_commerce_coupon_redeem_error_alter passed validation if we get to this point.
  return true;
}

/**
 * Implements hook_commerce_coupon_redeem_error_alter
 */
function coupon_combine_condition_commerce_coupon_redeem_error_alter(&$error, $coupon, $order) {

  $discount = commerce_coupon_discount_load_multiple(array($coupon->commerce_discount_reference['und'][0]['target_id']));
  $discount = reset($discount);

  $result = coupon_combine_condition_validate_discount_combinations($discount, $order->order_id);
  if($result !== true) {
    if($result === false) {
      $error = t('Discount cannot be combined with other offers.');
    }
    else {
      $error = t('Discount cannot be combined with: @name.', array('@name' => '"' . t($result) . '"'));
    }
  }
}

/**
 * Evaluate the discount being applied against existing discounts if applicable.
 * Returns true if allowed to add discount, else false.
 *
 * @param $active_discount
 * @param $order_id
 * @return bool|string Returns TRUE if successfully applied, returns FALSE if discount being applied isn't compatible,
 * returns the discount name if there is an already applied discount that is not compatible.
 */
function coupon_combine_condition_validate_discount_combinations($active_discount, $order_id) {

  $discounts = db_select('field_data_commerce_discounts', 'd')
    ->fields('d', array('commerce_discounts_target_id'))
    ->condition('entity_id', $order_id, '=')
    ->condition('commerce_discounts_target_id', $active_discount->discount_id, '!=')
    ->execute()
    ->fetchAssoc();

  // If no other discounts, then nothing to validate, return success.
  if(empty($discounts)) {
    return true;
  }

  $discounts = entity_load('commerce_discount', $discounts);
  if(empty($discounts)) {
    return true;
  }

  foreach($discounts as $id => $discount) {
    // First check if the discount being added works with this discount that has already been applied.
    if(stripos($active_discount->inline_conditions['und'][0]['condition_name'], 'coupon_combine_condition') !== false && array_search($discount->type, $active_discount->inline_conditions['und'][0]['condition_settings']['not_applicable_with']) !== false) {
      return false;
    }

    // Check if existing
    if(!empty($discount->inline_conditions['und'])) {
      // Loop over the discount's inline_conditions and evaluate against its coupon_combine_condition if applicable.
      foreach($discount->inline_conditions['und'] as $i => $condition) {
        // Is this condition a coupon_combine_condition?
        if(stripos($condition['condition_name'], 'coupon_combine_condition') !== false) {
          if(array_search($active_discount->type, $condition['condition_settings']['not_applicable_with']) !== false) {
            return ($discount->component_title . '');
          }
        }
      }
    }
  }

  return true;
}

/**
 * The callback for our custom inline_condition form.
 *
 * @param $settings
 * @return array
 */
function coupon_combine_condition_no_other_offer_configure($settings) {
  return array(
    'not_applicable_with' => array(
      '#type' => 'checkboxes',
      '#attributes' => array(
        'class' => array(
          'no-combine'
        )
      ),
      '#title' => t('Not applicable with:'),
      '#options' => array('order_discount' => t('Order Discounts'), 'product_discount' => t('Product Discounts')),
      '#default_value' => (!empty($settings['not_applicable_with']) ? $settings['not_applicable_with'] : array()),
    ),
  );
}